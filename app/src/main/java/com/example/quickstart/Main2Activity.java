package com.example.quickstart;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class Main2Activity extends AppCompatActivity {

    private Button btnPage1;
    private ImageView imgCalendrier;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        //Associer la variable à la vue.
        btnPage1 = findViewById(R.id.btnPage1);
        imgCalendrier = findViewById(R.id.imgCalendrier);

        //Crée un "Listener" pour le bouton btnPage1
        btnPage1.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {page1();}
        });

        //Crée un "Listener" pour le bouton btnPage1
        imgCalendrier.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {calendrier();}
        });
    }

    //Crée un nouvel "Intent" qui permet de changer d' "activity"
    private void page1()
    {
        Intent intent = new Intent(Main2Activity.this, MainActivity.class);
        startActivity(intent);

    }

    private void calendrier(){

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_APP_CALENDAR);
        startActivity(intent);
    }

}
package com.example.quickstart;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button btnPage2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Associer la variable à la vue.
        btnPage2 = findViewById(R.id.btnPage2);

        //Crée un "Listener" pour le bouton btnPage2
        btnPage2.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {page2();}
        });
    }

    //Crée un nouvel "Intent" qui permet de changer d' "activity"
    private void page2()
    {
        Intent intent = new Intent(MainActivity.this, Main2Activity.class);
        startActivity(intent);

    }
}
